
"""Python library presenting results from »ip addr«."""

# Author: 2020 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


__all__ = ['get_ipv6']


from collections.abc import Sequence, Mapping
import json
import subprocess


long_description = """
    Fundamental facilities for querying network interfaces
    using »ip addr«.
"""


def get_ipv6(interface: str) -> tuple[Sequence[Mapping[str, str]], Mapping[str, object]]:

    result = subprocess.run('ip -f inet6 -json address show dev'.split() + [interface],
      capture_output = True, encoding = 'utf-8', check = True)
    all_data = json.loads(result.stdout)[0]
    addr_info = all_data.pop('addr_info')

    return addr_info, all_data
